package com.legendary.reactive.api;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration
public class Router {
    @Bean
    public RouterFunction<ServerResponse> sportsRouterFunction(Handler handler) {
        return route()
                .GET("/api/v1/sport", handler::getByName)
                .POST("/api/v1/sport/{sportname}", handler::save)
                .build();
    }
}
