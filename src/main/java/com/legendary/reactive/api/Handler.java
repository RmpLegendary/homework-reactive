package com.legendary.reactive.api;

import com.legendary.reactive.model.Sport;
import com.legendary.reactive.repository.SportRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.server.ServerWebInputException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Optional;
import java.util.function.Predicate;

import static org.springframework.web.reactive.function.server.ServerResponse.ok;

@Component
@RequiredArgsConstructor
public class Handler {
    private final SportRepository sportRepository;

    public Mono<ServerResponse> getByName(ServerRequest request) {
        String name = Optional.ofNullable(request.queryParams().getFirst("q"))
                .filter(Predicate.not(String::isBlank))
                .orElseThrow(() -> new ServerWebInputException("wrong request"));

        Flux<Sport> sports = sportRepository.findAllByName(name);
        return ok().body(sports, Sport.class);
    }


    public Mono<ServerResponse> save(ServerRequest request) {
        String name = Optional.of(request.pathVariable("sportname"))
                .filter(Predicate.not(String::isBlank))
                .orElseThrow(() -> new ServerWebInputException("invalid sport name"));
        return sportRepository.findFirstByName(name)
                .flatMap(sport -> ServerResponse.unprocessableEntity().bodyValue("this sport name is already exist"))
                .switchIfEmpty(sportRepository.save(Sport.builder().name(name).build()).flatMap(sport -> ok().build()));
    }
}

