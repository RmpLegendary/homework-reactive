package com.legendary.reactive.service;

import com.legendary.reactive.model.Sport;
import reactor.core.publisher.Flux;

public interface SportApiClient {
    Flux<Sport> getAllSportData();
}
