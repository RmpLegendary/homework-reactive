package com.legendary.reactive.service;

import com.legendary.reactive.repository.SportRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.SmartLifecycle;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class SportApiConsumer implements SmartLifecycle {
    private final SportApiClient sportApiClient;
    private final SportRepository sportRepository;

    @Override
    public void start() {

        sportApiClient.getAllSportData().log().limitRate(20)
                .flatMap(sportRepository::insert).blockLast();
    }

    @Override
    public void stop() {

    }

    @Override
    public boolean isRunning() {
        return false;
    }
}
