package com.legendary.reactive.service.impl;

import com.legendary.reactive.model.Sport;
import com.legendary.reactive.model.SportData;
import com.legendary.reactive.service.SportApiClient;
import com.legendary.reactive.mapper.SportSourceMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

@Slf4j
@Service
@RequiredArgsConstructor
public class SportApiClientImpl implements SportApiClient {

    private final WebClient webClient;

    @Override
    public Flux<Sport> getAllSportData() {
        return webClient.get().uri("/sports")
                .exchangeToMono(resp -> resp.bodyToMono(SportData.class))
                .flatMapIterable(SportData::getData)
                .doOnError(err -> log.error("error getting sports from api", err))
                .doOnNext(data -> log.info("sports data: {}", data))
                .map(SportSourceMapper::map);
    }
}
