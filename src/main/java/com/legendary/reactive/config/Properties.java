package com.legendary.reactive.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix ="web-client")
public class Properties {

    private String baseUrl;
}