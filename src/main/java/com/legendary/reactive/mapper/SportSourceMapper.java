package com.legendary.reactive.mapper;

import com.legendary.reactive.model.Sport;
import com.legendary.reactive.model.SportSource;

public class SportSourceMapper {

    public static final String NAME = "name";

    public static Sport map(SportSource sportsSource){
        return Sport.builder()
                .id(sportsSource.getId())
                .name((String)sportsSource.getAttributes().get(NAME))
                .build();
    }
}
