package com.legendary.reactive.repository;

import com.legendary.reactive.model.Sport;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@Repository
public interface SportRepository extends ReactiveMongoRepository<Sport, String> {
    Mono<Sport> findFirstByName(String name);
    Flux<Sport> findAllByName(String name);
}
