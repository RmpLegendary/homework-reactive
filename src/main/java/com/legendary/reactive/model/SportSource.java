package com.legendary.reactive.model;

import lombok.Data;

import java.util.Map;

@Data
public class SportSource {
    private String id;
    private String type;
    private Map<String, Object> attributes;
}
