package com.legendary.reactive.model;

import lombok.Data;

import java.util.List;

@Data
public class SportData {
    private List<SportSource> data;
}
